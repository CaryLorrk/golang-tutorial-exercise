package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (self *rot13Reader) Read(p []byte) (n int, err error) {
	n, err = self.r.Read(p)
	for i := 0; i < n; i++ {
		p[i] = rot13(rune(p[i]))
	}
	return
}

func rot13(c rune) byte {
	switch {
	case c >= 'A' && c <= 'Z':
		c = (c-'A'+13)%26 + 'A'
	case c >= 'a' && c <= 'z':
		c = (c-'a'+13)%26 + 'a'
	}
	return byte(c)
}

func main() {
	s := strings.NewReader(
		"Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}

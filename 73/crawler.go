package main

import (
	"fmt"
	"sync"
)

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func Crawl(
	url string,
	depth int,
	fetcher Fetcher,
	ch chan string,
	root bool,
	lock *sync.Mutex,
	visited map[string]bool,
	pwg *sync.WaitGroup) {
	if depth <= 0 {
		pwg.Done()
		return
	}
	body, urls, err := fetcher.Fetch(url)
	if err != nil {
		fmt.Println(err)
		pwg.Done()
		return
	}
	ch <- fmt.Sprintf("found: %s %q\n", url, body)
	wg := new(sync.WaitGroup)
	for _, u := range urls {
		lock.Lock()
		if visited[u] {
			lock.Unlock()
			continue
		} else {
			visited[u] = true
			lock.Unlock()
		}
		wg.Add(1)
		go Crawl(u, depth-1, fetcher, ch, false, lock, visited, wg)
	}
	wg.Wait()
	if root {
		close(ch)
	}
	pwg.Done()
	return
}

func main() {
	visited := make(map[string]bool)
	ch := make(chan string, 10)
	lock := new(sync.Mutex)
	url := "http://golang.org/"
	wg := new(sync.WaitGroup)
	wg.Add(1)
	visited[url] = true
	go Crawl(url, 4, fetcher, ch, true, lock, visited, wg)
	for str := range ch {
		fmt.Print(str)
	}
	wg.Wait()
}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"http://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"http://golang.org/pkg/",
			"http://golang.org/cmd/",
		},
	},
	"http://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"http://golang.org/",
			"http://golang.org/cmd/",
			"http://golang.org/pkg/fmt/",
			"http://golang.org/pkg/os/",
		},
	},
	"http://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
	"http://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
}

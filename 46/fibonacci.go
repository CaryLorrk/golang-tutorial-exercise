package main

import "fmt"

func fibonacci() func() int {
	prev := 0
	num := 1
	return func() int {
		res := prev
		tmp := num
		num += prev
		prev = tmp
		return res
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}

package main

import (
	"code.google.com/p/go-tour/pic"
	"image"
	"image/color"
)

type Image struct {
}

func (self Image) ColorModel() color.Model {
	return color.RGBAModel
}

func (self Image) Bounds() image.Rectangle {
	return image.Rect(0, 0, 255, 255)
}

func (self Image) At(x, y int) color.Color {
	return color.RGBA{uint8(x ^ y), uint8(x * y), 255, 255}
}

func main() {
	m := Image{}
	pic.ShowImage(m)
}

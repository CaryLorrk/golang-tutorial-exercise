package main

import (
	"fmt"
	"math"
)

type ErrNegativeSqrt float64

func (e ErrNegativeSqrt) Error() string {
	return "not support negative number"
}

func Sqrt(x float64) (float64, error) {
	if x < 0 {
		return 0, ErrNegativeSqrt(1)
	}
	z0 := x
	z := z0 - ((z0*z0 - x) / (2 * z0))
	for math.Abs(z-z0) > 0.000001 {
		z0 = z
		z = z0 - ((z0*z0 - x) / (2 * z0))
	}
	return z0, nil
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}

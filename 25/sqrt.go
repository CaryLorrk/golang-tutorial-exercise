package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) float64 {
	z0 := x
	z := z0 - ((z0*z0 - x) / (2 * z0))
	for math.Abs(z-z0) > 0.000001 {
		z0 = z
		z = z0 - ((z0*z0 - x) / (2 * z0))
	}
	return z0
}

func main() {
	fmt.Println(Sqrt(2))
}

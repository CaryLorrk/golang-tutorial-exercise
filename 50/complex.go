package main

import (
	"fmt"
	"math"
	"math/cmplx"
)

func Cbrt(x complex128) complex128 {
	z0 := x
	z := z0 - ((cmplx.Pow(x, 3) - x) / (3 * cmplx.Pow(x, 2)))
	for math.Abs(real(z)-real(z0)) > 0.0000001 ||
		math.Abs(imag(z)-imag(z0)) > 0.0000001 {
		z0 = z
		z = z0 - ((cmplx.Pow(z0, 2) - x) / (2 * z0))
	}
	return z0
}

func main() {
	fmt.Println(Cbrt(2))
}
